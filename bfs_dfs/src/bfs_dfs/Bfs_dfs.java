/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfs_dfs;
import graph_library.*;
/**
 *
 * @author GPU_CIC 2
 */
public class Bfs_dfs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        graph grafo= graph_generators.Gnm(20,40, false, false, "Grafo Gnm 20");
       //print_graph(grafo_1);
       //graph_generators.export_GV(grafo_1);
       graph bfs= graph_generators.bfs(grafo,0);
       graph dfs_r = graph_generators.dfs_r(grafo,0);
       graph dfs_i = graph_generators.dfs_i(grafo, 0);
       
       print_graph(grafo);

        print_graph(bfs);
        print_graph(dfs_r);

        print_graph(dfs_i);

        graph_generators.export_GV(grafo);
        graph_generators.export_GV(dfs_r);
        graph_generators.export_GV(dfs_i);
        graph_generators.export_GV(bfs);
    }
    
    static void print_graph(graph grafo_1)
    {
        System.out.println(grafo_1.get_name());        

        System.out.println("Nodos:");
        
        grafo_1.nodes.entrySet().forEach((entry) -> {
            System.out.println("Nodo: " + entry.getKey() + ", Nombre: " + entry.getValue().get_name() + ", Grado: " + entry.getValue().get_grade()) ;
        });
        
        System.out.println("Aristas:");
        
       grafo_1.edges.entrySet().forEach((entry) -> {
            System.out.println("Arista: " + entry.getKey() + ", Nombre: " + entry.getValue().get_name());
        });
    }
    
}
