/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;
import java.util.HashMap;

/**
 *
 * @author GPU_CIC 2
 */

//import Math
public class graph {
    //private final boolean directed;
    private final String name;
    public final HashMap < Integer ,node> nodes=new HashMap<>();
    public final HashMap < String, edge> edges=new HashMap<>();
    private final boolean directed;
    private final boolean autocycle;
    
    public graph (String name,boolean directed,boolean autocycle ){
        this.name=name;
        this.directed=directed;
        this.autocycle=autocycle;
    }
   
    public String get_name(){
        return name;
    }

    public boolean is_directed(){
        return directed;
    }

    public boolean is_autocycle(){
        return autocycle;
    }
}
