/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 *
 * @author GPU_CIC 2
 */



public class graph_generators {

    static public graph Gnm( int n,int m, boolean directed, boolean autocycle, String name ){
        graph grafo= new graph(name,directed,autocycle);

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }      
        int node_1;
        int node_2;
        for(int i=0;i<m;i++){
            do{
                do{
                    node_1 = (int)(Math.random()*n);
                    node_2 = (int)(Math.random()*n);    
                }while(node_1==node_2 && !autocycle);
            }while(check_rep(node_1,node_2,grafo,directed));

            grafo.edges.put(node_1+","+node_2,new edge(grafo.nodes.get(node_1),grafo.nodes.get(node_2),"Arista "+node_1+","+node_2));
        }
        return grafo;
    }


    static public graph Gnp( int n, double p, boolean directed, boolean autocycle, String name ){
        graph grafo= new graph(name,directed,autocycle);

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }    

        for (int i=1; i<n;i++ ) {
            for (int j=0;j<i ;j++ ) {
                if(p>=Math.random()){
                    grafo.edges.put(i+","+j,new edge(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
                }
            }
        }

        if(autocycle){
            for (int i=0;i<n;i++ ) {
                if(p>=Math.random()){
                  grafo.edges.put(i+","+i,new edge(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
                }
            }
        } 

        if (directed) {
             for (int j=1; j<n;j++ ) {
                for (int i=0;i<j ;i++ ) {
                    if(p>=Math.random()){
                        grafo.edges.put(i+","+j,new edge(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
                    }
                }
            }
        } 
        return grafo;
    }

    static public graph Gnr( int n, double r, boolean directed, boolean autocycle, String name ){
        graph grafo= new graph(name,directed,autocycle);

        ArrayList <cordinate> cordinates = new ArrayList <>();

        //double coordenadas[][]= new double [n][2];

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
            double x=Math.random();
            double y=Math.random();
            cordinates.add(new cordinate(x,y));
        }    

        for (int i=1; i<n;i++ ) {
            for (int j=0;j<i ;j++ ) {  
                double d=Math.sqrt(Math.pow(cordinates.get(i).get_x()-cordinates.get(j).get_x(),2)+Math.pow(cordinates.get(i).get_y()-cordinates.get(j).get_y(),2));
                if(d<=r){
                    grafo.edges.put(i+","+j,new edge(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j)); 
                    if (directed) {
                        grafo.edges.put(j+","+i,new edge(grafo.nodes.get(j),grafo.nodes.get(i),"Arista "+j+","+i)); 
                    }
                }
            }
        }

        if(autocycle){
            for (int i=0;i<n;i++ ) {
                  grafo.edges.put(i+","+i,new edge(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
            }
        } 
        return grafo;
    }    

    static public graph Gnd( int n, int d, boolean directed, boolean autocycle, String name ){
        graph grafo= new graph(name,directed,autocycle);

        for (int i=0;i<d;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }    

        for (int i=1; i<d;i++ ) {
            for (int j=0;j<i ;j++ ) {
                grafo.edges.put(i+","+j,new edge(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
           /*     if (directed) {
                    grafo.edges.put(j+","+i,new edge(grafo.nodes.get(j),grafo.nodes.get(i),"Arista "+j+","+i)); 
                }*/
            }
        }

        while (grafo.nodes.size()<n) {
            grafo.nodes.put(grafo.nodes.size(),new node("Nodo "+(grafo.nodes.size())));
            for (int i=0;i<grafo.nodes.size()-1 ;i++) {
                double p=1-((double)grafo.nodes.get(i).get_grade()/(double)d); 
                double r=Math.random();

                //System.out.println(i+","+String.valueOf(p)+","+String.valueOf(r));
                if (p>r) {
                    grafo.edges.put(i+","+(grafo.nodes.size()-1),new edge(grafo.nodes.get(i),grafo.nodes.get(grafo.nodes.size()-1),"Arista "+i+","+(grafo.nodes.size()-1)));
                }
            }
        }

       /* if(autocycle){
            for (int i=0;i<d;i++ ) {
                  grafo.edges.put(i+","+i,new edge(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
            }
        }*/ 
        return grafo;
    }
        
    static boolean check_rep(int node_1, int node_2,graph grafo, boolean directed){
        
        boolean repeted=false;
                
        for (String key : grafo.edges.keySet()) {
            
            if (key.equals(node_1+","+node_2)||key.equals(node_2+","+node_1)&&!directed) {
                 repeted=true; 
            }
        }          
        return repeted;
    }

    static public graph bfs(graph father, int root)

    {
        if (father.nodes.containsKey(root)) {
            graph tree= new graph(father.get_name()+"_bfs",father.is_directed(),father.is_autocycle());

            HashMap < Integer ,node> Layer=new HashMap<>();
            HashMap < Integer ,node> Layer_sig=new HashMap<>();
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }

            not_discovered.remove(root);

            Layer.put(root,new node("Nodo "+root));
            tree.nodes.put(root,new node("Nodo "+root));


            while(!Layer.isEmpty()){
                for (int key_node : Layer.keySet() ) {
                    for (String key_edge : father.edges.keySet()) {
                        int node_1=  father.edges.get(key_edge).get_node_1().get_key();
                        int node_2= father.edges.get(key_edge).get_node_2().get_key();
                        if (key_node==node_1&&not_discovered.containsKey(node_2)){
                            not_discovered.remove(node_2);
                            Layer_sig.put(node_2,new node("Nodo "+node_2));
                            tree.nodes.put(node_2,new node("Nodo "+node_2));
                            tree.edges.put(node_1+","+node_2,new edge(tree.nodes.get(node_1),tree.nodes.get(node_2),"Arista "+node_1+","+node_2));
                        }
                        if (key_node==node_2&&not_discovered.containsKey(node_1)) {
                            not_discovered.remove(node_1);
                            Layer_sig.put(node_1,new node("Nodo "+node_1));
                            tree.nodes.put(node_1,new node("Nodo "+node_1));
                            tree.edges.put(node_2+","+node_1,new edge(tree.nodes.get(node_2),tree.nodes.get(node_1),"Arista "+node_2+","+node_1));
                        }
                    } 
                }

                Layer.clear();

                for (int key_layer : Layer_sig.keySet() ) {
                    Layer.put(key_layer,Layer_sig.get(key_layer));
                }

                Layer_sig.clear();
            }

            return tree;      
        }
        else
        {
            return null;
        }
    }

    static public graph dfs_r(graph father, int root)
    {
        if (father.nodes.containsKey(root)){
            graph tree= new graph(father.get_name()+"_dfs_r",father.is_directed(),father.is_autocycle());
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }

            not_discovered.remove(root);

            tree.nodes.put(root,new node("Nodo "+root));

            dfs_recursive_call(father,root,tree,not_discovered);

            return tree;
        }
        else
        {
            return null;
        }

    }


    static private void dfs_recursive_call(graph father, int node, graph tree, HashMap not_discovered)
    {
        for (String key_edge : father.edges.keySet()) {
            int node_1=  father.edges.get(key_edge).get_node_1().get_key();
            int node_2= father.edges.get(key_edge).get_node_2().get_key();
            if (node==node_1&&not_discovered.containsKey(node_2)){
                not_discovered.remove(node_2);
                tree.nodes.put(node_2,new node("Nodo "+node_2));
                tree.edges.put(node_1+","+node_2,new edge(tree.nodes.get(node_1),tree.nodes.get(node_2),"Arista "+node_1+","+node_2));
                dfs_recursive_call(father,node_2,tree,not_discovered);
            }
            if (node==node_2&&not_discovered.containsKey(node_1)) {
                not_discovered.remove(node_1);
                tree.nodes.put(node_1,new node("Nodo "+node_1));
                tree.edges.put(node_2+","+node_1,new edge(tree.nodes.get(node_2),tree.nodes.get(node_1),"Arista "+node_2+","+node_1));
                dfs_recursive_call(father,node_1,tree,not_discovered);
            } 
        }
    }


    static public graph dfs_i (graph father, int root)
    {
        if (father.nodes.containsKey(root)){
            graph tree= new graph(father.get_name()+"_dfs_i",father.is_directed(),father.is_autocycle());
            
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }
            //not_discovered.remove(root);
            Stack pila = new Stack();
            
            boolean valid=false;

            par it = new par(root,0); 
            pila.push(it);

            while(!pila.empty())
            {
                it = (par) pila.pop();
                int anterior = it.get_b();
                int nodo = it.get_a();
                if (not_discovered.containsKey(nodo)) {
                    not_discovered.remove(nodo);
                    tree.nodes.put(nodo,new node("Nodo "+nodo));
                    if (valid) {
                        tree.edges.put(anterior+","+nodo,new edge(tree.nodes.get(anterior),tree.nodes.get(nodo),"Arista "+anterior+","+nodo));
                    }
                    valid=true;
                    for (String key_edge : father.edges.keySet())  {
                        int node_1=  father.edges.get(key_edge).get_node_1().get_key();
                        int node_2= father.edges.get(key_edge).get_node_2().get_key();
                        if (nodo==node_1){
                            pila.push(new par(node_2,node_1));
                        }
                        if (nodo==node_2&&not_discovered.containsKey(node_1)) {
                            pila.push(new par(node_1,node_2));
                        }
                    }
                }
            }
            
            return tree;
        }
        else
        {
            return null;
        }
    }

    static public void export_GV(graph grafo){
        FileWriter fichero=null;
        PrintWriter pw;
        try
        {
            fichero = new FileWriter(grafo.get_name().replace(" ","_")+".gv");
            pw = new PrintWriter(fichero);

            String bufer;
            if (grafo.is_directed()) {
                bufer= "digraph ";
            }else{

                bufer= "graph ";
            }

            bufer = bufer + grafo.get_name().replace(" ","_");
            
            pw.println(bufer);
            pw.println("{");

            grafo.nodes.entrySet().forEach((entry) -> {
                pw.println("    "+entry.getKey()+";");
            });
 
            grafo.edges.entrySet().forEach((entry) -> {

                String key=entry.getKey();
                String bufer_2;
                int i=0;

                while(key.charAt(i)!=','){
                    i++;
                }

                bufer_2="    "+key.substring(0,i);

                if (grafo.is_directed()) {
                    bufer_2=bufer_2+"->";                    
                }else{
                    bufer_2=bufer_2+"--";   
                }           
                bufer_2=bufer_2+key.substring(i+1)+";";

                pw.println(bufer_2);
            });
           pw.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}


