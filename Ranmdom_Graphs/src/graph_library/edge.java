/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;
/**
 *
 * @author GPU_CIC 2
 */
public class edge {
    //private final boolean directed;
    private final node    node_1;
    private final node    node_2;
   // private final int id;
    private final String name;


    
    public edge(node node_1, node node_2, String name){
        //this.directed = directed;
     //   this.id= id;
        this.node_1=node_1;
        this.node_2=node_2;
        this.name=name;
        node_1.inc_grade();
        if(node_1!=node_2){
            node_2.inc_grade();
        }
    }
    
    public String get_name(){
        return name;
    }
    
    public node get_node_1(){
        return node_1;
    }
    
    public node get_node_2(){
        return node_2;
    }
}
